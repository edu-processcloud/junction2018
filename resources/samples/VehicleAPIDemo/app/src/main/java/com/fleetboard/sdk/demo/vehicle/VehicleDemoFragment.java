package com.fleetboard.sdk.demo.vehicle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;


public class VehicleDemoFragment extends ListFragment {

    private static final Comparator<VehicleDemoListItem> SORT_TOPIC_ASC = new Comparator<VehicleDemoListItem>() {
        @Override
        public int compare(final VehicleDemoListItem lhs, final VehicleDemoListItem rhs) {
            return lhs.getSwitchLabel() - rhs.getSwitchLabel();
        }
    };
    private static final Comparator<VehicleDemoListItem> SORT_TOPIC_DESC = new Comparator<VehicleDemoListItem>() {
        @Override
        public int compare(final VehicleDemoListItem lhs, final VehicleDemoListItem rhs) {
            return rhs.getSwitchLabel() - lhs.getSwitchLabel();
        }
    };

    private List<VehicleDemoListItem> fItems;
    private VehicleDemoListAdapter fAdapter;
    private TextView fTopicLabel;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_vehicle_listfragment, container, false);
        fTopicLabel = (TextView) view.findViewById(R.id.vehicle_ListHeaderTopic);
        fTopicLabel.setTag("ASC");
        fTopicLabel.setOnClickListener(topicListener());
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fItems = VehicleDemoListItem.makeList(getActivity());
        fAdapter = new VehicleDemoListAdapter(getActivity(), R.layout.activity_vehicle_listview_item,
                new ArrayList<VehicleDemoListItem>());
        fAdapter.addAll(fItems);
        setListAdapter(fAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
        fAdapter.disconnectVehicleClient();
    }

    @Override
    public void onResume() {
        super.onResume();
        fAdapter.connectVehicleClient();
        fAdapter.notifyDataSetChanged();
    }

    private OnClickListener topicListener() {
        return new OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String sort = fTopicLabel.getTag().toString();
                if ("ASC".equals(sort)) {
                    fTopicLabel.setTag("DESC");
                    fTopicLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                            android.R.drawable.arrow_up_float, 0);
                    fAdapter.sort(SORT_TOPIC_DESC);
                } else {
                    fTopicLabel.setTag("ASC");
                    fTopicLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                            android.R.drawable.arrow_down_float, 0);
                    fAdapter.sort(SORT_TOPIC_ASC);
                }
                fAdapter.notifyDataSetChanged();
            }
        };
    }
}
