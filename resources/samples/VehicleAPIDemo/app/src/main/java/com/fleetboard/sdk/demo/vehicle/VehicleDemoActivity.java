package com.fleetboard.sdk.demo.vehicle;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import com.fleetboard.sdk.lib.android.log.Log;

public class VehicleDemoActivity extends Activity {

    private static final String TAG = VehicleDemoActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_vehicle);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "in Switch: " + item.getItemId());
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(MainActivity.getMainActivityIntent(this));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
