package com.fleetboard.sdk.demo.vehicle;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.fleetboard.sdk.lib.android.log.Log;
import com.fleetboard.sdk.lib.android.vehicle.IVehicleClientCallback;
import com.fleetboard.sdk.lib.android.vehicle.VehicleClient;
import com.fleetboard.sdk.lib.android.vehicle.VehicleClientException;
import com.fleetboard.sdk.lib.common.OnOffState;
import com.fleetboard.sdk.lib.vehicle.IVehicleError;
import com.fleetboard.sdk.lib.vehicle.IVehicleMessage;
import com.fleetboard.sdk.lib.vehicle.ValidState;

public class VehicleDemoListAdapter extends ArrayAdapter<VehicleDemoListItem> implements
        IVehicleClientCallback {

    private static final String TAG = VehicleDemoActivity.class.getSimpleName();
    private static final Object MONITOR = new Object();

    private final Context fContext;
    private final List<VehicleDemoListItem> fData;

    public VehicleDemoListAdapter(final Context context, final int layoutResourceId,
                                  final List<VehicleDemoListItem> data) {
        super(context, 0, data);
        fContext = context;
        fData = data;
    }

    public void connectVehicleClient() {
        try {
            VehicleClient.INSTANCE.connect(this, fContext.getApplicationContext());
        } catch (Exception e) {
            Log.e(TAG, "connectVehicleClient failed", e);
            Toast.makeText(fContext, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void disconnectVehicleClient() {
        VehicleClient.INSTANCE.disconnect();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.activity_vehicle_listview_item, parent, false);
        }
        updateRow(getItem(position), convertView);
        return convertView;
    }

    /* (non-Javadoc)
     * @see com.fleetboard.sdk.lib.android.vehicle.IVehicleClientCallback#handleVehicleMessage(com.fleetboard.sdk.lib.vehicle.IVehicleMessage)
     */
    @Override
    public void handleVehicleMessage(IVehicleMessage message) {
        Log.i(TAG, "handleVehicleMessage: " + message);
        synchronized (MONITOR) {
            for (final VehicleDemoListItem item : fData) {
                if (message.getTopic() == item.getTopic() && item.isActive() && item.isEnabled()) {
                    item.handleMessage(message);
                    notifyDataSetChanged();
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.fleetboard.sdk.lib.android.vehicle.IVehicleClientCallback#onError(com.fleetboard.sdk.lib.vehicle.IVehicleError)
     */
    @Override
    public void onError(IVehicleError error) {
        Log.e(TAG, "onError: " + error);
    }

    /* (non-Javadoc)
     * @see com.fleetboard.sdk.lib.android.vehicle.IVehicleClientCallback#onServiceConnected()
     */
    @Override
    public void onVehicleClientConnected() {
        Log.i(TAG, "onServiceConnected");
        for (final VehicleDemoListItem item : fData) {
            // automatically active item (=> switch will be checked) and register for topic
            item.setEnabled(true);
            item.setActive(true);
            registerForTopic(item.getTopic());
        }
        notifyDataSetChanged();
    }

    /* (non-Javadoc)
     * @see com.fleetboard.sdk.lib.android.vehicle.IVehicleClientCallback#onServiceDisconnected()
     */
    @Override
    public void onVehicleClientDisconnected() {
        Log.i(TAG, "onServiceDisconnected");
        for (final VehicleDemoListItem item : fData) {
            // automatically deactive item (=> switch will be unchecked) and unregister from topic
            item.setActive(false);
            item.setEnabled(false);
            unregisterFromTopic(item.getTopic());
            item.reset();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void updateRow(final VehicleDemoListItem item, final View view) {
        resetVisibility(view);
        updateSwitch(item, view);
        updateContent(item, view);
    }

    private void updateSwitch(final VehicleDemoListItem item, final View view) {
        final Switch switchItem = (Switch) view.findViewById(R.id.vehicle_switchItem);
        switchItem.setPressed(true);
        switchItem.setTag(item);
        switchItem.setText(fContext.getText(item.getSwitchLabel()));
        switchItem.setEnabled(item.isEnabled());
        switchItem.setChecked(item.isActive());
        switchItem.setOnCheckedChangeListener(switchChangeListener(view));
    }

    private OnCheckedChangeListener switchChangeListener(final View view) {
        return new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setPressed(true);
                final VehicleDemoListItem item = (VehicleDemoListItem) buttonView.getTag();
                if (isChecked) {
                    registerForTopic(item.getTopic());
                    item.setActive(true);
                } else {
                    unregisterFromTopic(item.getTopic());
                    item.reset();
                    resetVisibility(view);
                    notifyDataSetChanged();
                }
            }
        };
    }

    private void resetVisibility(final View view) {
        view.findViewById(R.id.vehicle_progressBarWrapper).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.vehicle_textViewItem).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.vehicle_statelistTypeWrapper).setVisibility(View.INVISIBLE);
    }

    private void updateContent(final VehicleDemoListItem item, final View view) {
        if (ValidState.VALID.equals(item.getValidity()))
            updateValidContent(item, view);
        else {
            final TextView textViewItem = (TextView) view.findViewById(R.id.vehicle_textViewItem);
            if (ValidState.ERROR.equals(item.getValidity())) {
                textViewItem.setText(view.getContext().getText(R.string.vehicle_DataError));
                textViewItem.setTextColor(Color.RED);
            } else if (ValidState.NA.equals(item.getValidity())) {
                textViewItem.setText(view.getContext().getText(R.string.vehicle_DataNA));
                textViewItem.setTextColor(Color.YELLOW);
            }
            if (item.isEnabled() && item.isActive())
                textViewItem.setVisibility(View.VISIBLE);
        }
    }

    private void updateValidContent(final VehicleDemoListItem item, final View view) {
        switch (item.getType()) {
            case TEXT:
                updateTextView(item, view);
                break;
            case PROGRESS_BAR:
                updateBarView(item, view);
                break;
            case STATELIST:
                updateStatelistView(item, view);
                break;
        }
    }

    private void updateTextView(final VehicleDemoListItem item, final View view) {
        final TextView textViewItem = (TextView) view.findViewById(R.id.vehicle_textViewItem);
        textViewItem.setText(item.getText());
        textViewItem.setTextColor(Color.WHITE);
        if (item.isEnabled() && item.isActive()) {
            textViewItem.setVisibility(View.VISIBLE);
        }
    }

    private void updateBarView(final VehicleDemoListItem item, final View view) {
        final ProgressBar bar = (ProgressBar) view.findViewById(R.id.vehicle_progressBar);
        final TextView textViewItem = (TextView) view.findViewById(R.id.vehicle_progressBarTxt);
        bar.setMax(item.getMaxProgress());
        bar.setProgress(item.getProgress());
        textViewItem.setText(item.getText());
        if (item.isEnabled() && item.isActive()) {
            final RelativeLayout rLayout = (RelativeLayout) view
                    .findViewById(R.id.vehicle_progressBarWrapper);
            rLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateStatelistView(final VehicleDemoListItem item, final View view) {
        final LinearLayout rLayout = (LinearLayout) view.findViewById(R.id.vehicle_statelistTypeWrapper);
        rLayout.removeAllViews();
        if (item.isEnabled() && item.isActive()) {
            for (Map.Entry<String, OnOffState> current : item.getStatelistMap().entrySet()) {
                final ToggleButton button = new ToggleButton(rLayout.getContext());
                final String onControl = view.getContext().getText(R.string.vehicle_SwitchOn).toString();
                final String onText = current.getKey().isEmpty() ? onControl : String.format("%s: %s",
                        current.getKey(), current.getValue().getName().toUpperCase());
                final String offControl = view.getContext().getText(R.string.vehicle_SwitchOff).toString();
                final String offText = current.getKey().isEmpty() ? offControl : String.format("%s: %s",
                        current.getKey(), current.getValue().getName().toUpperCase());
                button.setTextSize(20);
                button.setTextOn(onText);
                button.setTextOff(offText);
                button.setChecked(current.getValue() == OnOffState.ON);
                button.setClickable(false);
                rLayout.addView(button);
            }
            rLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Call the VehicleClient to register for the specified topic.
     *
     * @param topic The topic register for.
     */
    private void registerForTopic(int topic) {
        Log.d(TAG, "register for " + topic);
        try {
            VehicleClient.INSTANCE.registerForTopics(topic);
        } catch (VehicleClientException e) {
            Log.e(TAG, "registerForTopic failed", e);
        }
    }

    /**
     * Call the VehicleClient to unregister from the specified topic.
     *
     * @param topic The topic to unregister from.
     */
    private void unregisterFromTopic(int topic) {
        Log.d(TAG, "unregister from " + topic);
        try {
            VehicleClient.INSTANCE.unregisterFromTopic(topic);
        } catch (VehicleClientException e) {
            Log.e(TAG, "unregisterFromTopic failed", e);
        }
    }

}
