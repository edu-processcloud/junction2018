package com.fleetboard.sdk.demo.vehicle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.content.Context;


import com.fleetboard.sdk.lib.common.OnOffState;
import com.fleetboard.sdk.lib.vehicle.IVehicleMessage;
import com.fleetboard.sdk.lib.vehicle.PtoState;
import com.fleetboard.sdk.lib.vehicle.ValidState;
import com.fleetboard.sdk.lib.vehicle.VehicleTopicConsts;

public final class VehicleDemoListItem implements Comparable<VehicleDemoListItem> {

    public static enum Type {
        PROGRESS_BAR, STATELIST, TEXT;
    }

    private static final List<VehicleDemoListItem> ITEMS = new ArrayList<VehicleDemoListItem>();

    private Type fType;
    private ValidState fValidity;
    private int fSwitchLabel;
    private boolean fActive;
    private boolean fEnabled;
    private String fText;
    private final int fTopic;

    // PROGRESS_BAR only
    private int fMaxProgress;
    private int fProgress;
    // STATELIST only
    private Map<String, OnOffState> fStatelistMap = new TreeMap<String, OnOffState>();
    // STATELIST only
//	private Map<String, OnOffState> fTernaryStatelistMap = new TreeMap<String, OnOffState>();

    private VehicleDemoListItem(final Type type, final int switchLabel, final int topic) {
        fType = type;
        fValidity = ValidState.VALID;
        fSwitchLabel = switchLabel;
        fTopic = topic;
    }

    public synchronized String getText() {
        return fText;
    }

    public synchronized int getSwitchLabel() {
        return fSwitchLabel;
    }

    public synchronized Type getType() {
        return fType;
    }

    public synchronized ValidState getValidity() {
        return fValidity;
    }

    public synchronized int getTopic() {
        return fTopic;
    }

    public synchronized int getProgress() {
        if (Type.PROGRESS_BAR != fType) {
            throw new UnsupportedOperationException();
        }
        return fProgress;
    }

    public synchronized int getMaxProgress() {
        if (Type.PROGRESS_BAR != fType) {
            throw new UnsupportedOperationException();
        }
        return fMaxProgress;
    }

    private synchronized void setMaxProgress(int maxProgress) {
        if (Type.PROGRESS_BAR != fType) {
            throw new UnsupportedOperationException();
        }
        fMaxProgress = maxProgress;
    }

    public synchronized boolean isEnabled() {
        return fEnabled;
    }

    public synchronized void setEnabled(final boolean enabled) {
        fEnabled = enabled;
    }

    public synchronized void handleMessage(final IVehicleMessage message) {
        fValidity = message.getValidState();
        if (ValidState.VALID.equals(message.getValidState()))
            handleValidMessage(message);
    }

    private void handleValidMessage(final IVehicleMessage message) {
        if (message.getValue() == null)
            return;

        switch (fTopic) {
            case VehicleTopicConsts.DRIVER_ID1:
            case VehicleTopicConsts.DRIVER_ID2:
            case VehicleTopicConsts.VEHICLE_ID:
            case VehicleTopicConsts.FMS_MODULE_SW_VERSION:
                fText = message.getValueAsString();
                break;
            case VehicleTopicConsts.VEHICLE_SPEED:
                int value = message.getValueAsFloat().intValue();
                fText = String.format("%s km/h", value);
                fProgress = value;
                break;
            case VehicleTopicConsts.BRAKE_SWITCH_STATE:
            case VehicleTopicConsts.CRUISE_CONTROL_STATE:
            case VehicleTopicConsts.CLUTCH_SWITCH_STATE:
                fStatelistMap.put("", message.getValueAsOnOffState());
                break;
            case VehicleTopicConsts.COOLANT_TEMPERATURE:
            case VehicleTopicConsts.AMBIENT_TEMPERATURE:
                fText = String.format("%d \u00B0C", message.getValueAsInteger());
                break;
            case VehicleTopicConsts.CURRENT_FUEL_CONSUMPTION:
                fText = String.format("%.0f l/h", message.getValueAsFloat());
                fProgress = message.getValueAsFloat().intValue();
                break;
            case VehicleTopicConsts.ENGINE_SPEED:
                fText = String.format("%.0f rpm", message.getValueAsFloat());
                fProgress = message.getValueAsFloat().intValue();
                break;
            case VehicleTopicConsts.FUEL_LEVEL:
            case VehicleTopicConsts.ACCEL_PEDAL_POS:
                fText = String.format("%d %%", message.getValueAsInteger());
                fProgress = message.getValueAsInteger();
                break;
            case VehicleTopicConsts.PTO_STATE:
                final PtoState ptoState = message.getValueAsPtoState();
//			fTernaryStatelistMap.put(String.format("PTO-%d", ptoState.getNumber()),  ptoState.getState());
                fStatelistMap.put(String.format("PTO-%d", ptoState.getNumber()), ptoState.getState());
                break;
            case VehicleTopicConsts.TOTAL_ENGINE_HOURS:
                double time = message.getValueAsDouble();
                int hours = (int) time;
                int minutes = (int) ((time - hours) * 60);
                int seconds = (int) (((time - hours) * 60 - minutes) * 60);
                fText = String.format("%d h %02d m %02d s ", hours, minutes, seconds);
                break;
            case VehicleTopicConsts.TOTAL_FUEL_USED:
                fText = String.format("%.0f l", message.getValueAsFloat());
                break;
            case VehicleTopicConsts.TOTAL_VEHICLE_DISTANCE:
            case VehicleTopicConsts.SERVICE_DISTANCE:
                fText = String.format("%d km", message.getValueAsLong());
                break;
            case VehicleTopicConsts.VEHICLE_WEIGHT:
                fText = String.format("%.2f kg", message.getValueAsFloat());
                break;
        }
    }

    public synchronized boolean isActive() {
        return fActive;
    }

    public synchronized void setActive(boolean active) {
        fActive = active;
    }

    public synchronized Map<String, OnOffState> getStatelistMap() {
        if (Type.STATELIST != fType) {
            throw new UnsupportedOperationException();
        }
        return fStatelistMap;
    }


    public synchronized void reset() {
        fActive = false;
        fText = "";
        switch (fType) {
            case TEXT:
                break;
            case PROGRESS_BAR:
                fProgress = 0;
                break;
            case STATELIST:
                for (Map.Entry<String, OnOffState> entry : fStatelistMap.entrySet())
                    fStatelistMap.put(entry.getKey(), OnOffState.UNKNOWN);
                break;
        }
    }

    @Override
    public int compareTo(final VehicleDemoListItem other) {
        return fSwitchLabel - other.fSwitchLabel;
    }

    public static List<VehicleDemoListItem> makeList(final Context context) {
        synchronized (VehicleDemoListItem.class) {
            if (ITEMS.isEmpty()) {
                final String[] topics = context.getResources().getStringArray(R.array.vehicle_TopicList);
                for (String topic : topics) {
                    final String[] params = topic.split("\\|");
                    try {
                        ITEMS.add(makeListItem(params));
                    } catch (Exception e) {
                        //TODO: add better logging (?)
                        e.printStackTrace();
                    }
                }
                Collections.sort(ITEMS);
            }
            return Collections.unmodifiableList(ITEMS);
        }
    }

    private static VehicleDemoListItem makeListItem(String[] params) throws Exception {
        final Type type = Type.valueOf(params[0]);
        final int label = R.string.class.getField(params[1]).getInt(null);
        final int topic = Integer.parseInt(params[3]);
        VehicleDemoListItem listItem = new VehicleDemoListItem(type, label, topic);
        switch (type) {
            case TEXT:
                // nothing to add
                break;
            case PROGRESS_BAR:
                listItem.setMaxProgress(Integer.parseInt(params[4]));
                break;
            case STATELIST:
                String[] switches = Arrays.copyOfRange(params, 4, params.length);
                for (String switchLabel : switches)
                    listItem.getStatelistMap().put(switchLabel, OnOffState.UNKNOWN);
                break;
        }
        return listItem;
    }
}
