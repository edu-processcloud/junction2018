package com.fleetboard.sdk.demo.driverdistraction;

import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.fleetboard.sdk.lib.android.distraction.DriverDistractionClient;
import com.fleetboard.sdk.lib.android.distraction.DriverDistractionClientException;
import com.fleetboard.sdk.lib.android.distraction.IDriverDistractionClientCallback;
import com.fleetboard.sdk.lib.android.log.Log;

import com.fleetboard.sdk.lib.distraction.IDriverDistractionError;
import com.fleetboard.sdk.lib.distraction.IDriverDistractionEvent;
import com.fleetboard.sdk.lib.distraction.MovingState;
import com.fleetboard.sdk.lib.distraction.RestrictionState;


/**
 * Demo class to show an example usage of the Driver Distraction API. It visualizes
 * the currently known restriction and moving states as propagated by the API.
 * Also it reacts to the restriction state by disabling the UI elements, that
 * allow user input, whenever the restriction state indicates a restriction.
 */
public class DriverDistractionDemoActivity extends Activity implements IDriverDistractionClientCallback,
        OnCheckedChangeListener {

    private static final String TAG = DriverDistractionDemoActivity.class.getName();
    private EditText fInputText;
    private SeekBar fSeekBar;
    private Spinner fSpinner;
    private RadioGroup fRestLvlGroup;
    private RadioGroup fMvngStGroup;
    private Switch fEventSwitch;

    private boolean fRegisterListener = false;

    private DriverDistractionClient fDistractionClient;

    /**
     * Initialize the UI elements, register required callbacks and connect the
     * {@link DriverDistractionClient}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "OnCreate");
        super.onCreate(savedInstanceState);

        initUI();

        try {
            if (fDistractionClient == null)
                DriverDistractionClient.INSTANCE.connect(this, this);
            else
                Log.v(TAG, "Client already connected");
        } catch (DriverDistractionClientException e) {
            Log.e(TAG, "Could not init DriverDistraction client!", e);
        }
    }

    /**
     * Disconnect the {@link DriverDistractionClient}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "On Destroy");
        DriverDistractionClient.INSTANCE.disconnect();
    }

    /**
     * Handle incoming {@link IDriverDistractionEvent events} and update the UI
     * accordingly.
     * <p>
     * If the <code>null</code> is passed in, it is handled as an event with
     * states {@link MovingState#UNKNOWN} and {@link RestrictionState#UNKNOWN}.
     */
    @Override
    public void onDriverDistractionEvent(IDriverDistractionEvent distEvent) {
        Log.i(TAG, "Received DriverDistraction event");

        if (distEvent != null) {
            MovingState moving = distEvent.getMoving();
            handleMovingState(moving.getValue());

            RestrictionState restriction = distEvent.getRestriction();
            handleRestrictionState(restriction.getValue());
        } else {
            handleMovingState(MovingState.INT_UNKNOWN);
            handleRestrictionState(RestrictionState.INT_UNKNOWN);
        }
    }

    /**
     * As the client is now connected to the API, the event listener (this)
     * is registered.
     */
    @Override
    public void onDriverDistractionClientConnected() {
        Log.v(TAG, "Client ready");
        fDistractionClient = DriverDistractionClient.INSTANCE;
        fEventSwitch.setEnabled(true);
        if (fRegisterListener)
            fEventSwitch.setChecked(true);
    }

    /**
     * The client has been disconnected unexpectedly so we open up the UI
     * again.
     */
    @Override
    public void onDriverDistractionClientDisconnected() {
        Log.w(TAG, "Client disconnected");
        fRegisterListener = fEventSwitch.isChecked();
        fDistractionClient = null;
        fEventSwitch.setEnabled(false);
        fEventSwitch.setChecked(false);
        enableInput();
        fMvngStGroup.clearCheck();
        fRestLvlGroup.clearCheck();
    }

    /**
     * Currently no error might occur so we simply show the error message as Toast
     */
    @Override
    public void onError(IDriverDistractionError e) {
        Log.e(TAG, e.getErrorMessage());
        showToast("onError: " + e.getErrorMessage());
    }

    /**
     * Register or unregister the event listener. This could only fail, if the connection
     * to the API is not ready. However, in that case the button ought to be disabled,
     * therefore this event should never occur when the connection is lost.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            if (isChecked)
                fDistractionClient.registerDriverDistractionListener();
            else {
                fDistractionClient.unregisterDriverDistractionListener();
                onDriverDistractionEvent(null);
                disableInput();
            }
        } catch (DriverDistractionClientException e) {
            showToast("EventListener (de-)registration failed");
        }
    }

    /**
     * Dispatch the moving state and update the UI accordingly
     *
     * @param state the moving state
     */
    private void handleMovingState(int state) {
        switch (state) {
            case MovingState.INT_MOVING:
                fMvngStGroup.check(R.id.distractionMovingStateMoving);
                break;
            case MovingState.INT_STILL:
                fMvngStGroup.check(R.id.distractionMovingStateStill);
                break;
            case MovingState.INT_UNKNOWN:
            default:
                fMvngStGroup.check(R.id.distractionMovingStateUnknown);
                break;
        }
    }

    /**
     * Dispatch the restriction state and update the UI accordingly.
     * <p>
     * When the restriction state indicates an input restriction,
     * then the input UI elements are disabled.
     *
     * @param state the restriction state
     */
    private void handleRestrictionState(int state) {
        switch (state) {
            case RestrictionState.INT_OPEN:
                fRestLvlGroup.check(R.id.distractionRestrictionLevelOpen);
                enableInput();
                break;
            case RestrictionState.INT_RESTRICTED:
                fRestLvlGroup.check(R.id.distractionRestrictionLevelRestricted);
                disableInput();
                break;
            case RestrictionState.INT_UNKNOWN:
            default:
                fRestLvlGroup.check(R.id.distractionRestrictionLevelUnknown);
                enableInput();
                break;
        }
    }

    private void enableInput() {
        fSpinner.setEnabled(true);
        fSeekBar.setEnabled(true);
        fInputText.setEnabled(true);
    }

    private void disableInput() {
        fSpinner.setEnabled(false);
        fSeekBar.setEnabled(false);
        fInputText.setEnabled(false);
    }

    private void showToast(String message) {
        Toast.makeText(DriverDistractionDemoActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void initUI() {
        getActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_driverdistraction);

        fEventSwitch = (Switch) findViewById(R.id.driverDistractionEventSwitch);
        fEventSwitch.setOnCheckedChangeListener(this);
        fRestLvlGroup = (RadioGroup) findViewById(R.id.distractionRestrictionLevel);
        fMvngStGroup = (RadioGroup) findViewById(R.id.distractionMovingState);
        fInputText = (EditText) findViewById(R.id.distractionInputText);
        fSeekBar = (SeekBar) findViewById(R.id.distractionInputSeekBar);
        fSpinner = (Spinner) findViewById(R.id.distractionInputSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, Arrays.asList("Item 1", "Item 2", "Item 3", "Item 4"));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fSpinner.setAdapter(adapter);

        //force an event UNKNOWN,UNKNOWN and then disable input
        onDriverDistractionEvent(null);
        disableInput();
    }

}
