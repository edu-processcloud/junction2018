package com.fleetboard.sdk.demo.powerstate;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.fleetboard.sdk.lib.android.log.Log;
import com.fleetboard.sdk.lib.android.powerstate.IPowerStateClientCallback;
import com.fleetboard.sdk.lib.android.powerstate.PowerStateClient;
import com.fleetboard.sdk.lib.android.powerstate.PowerStateClientException;
import com.fleetboard.sdk.lib.common.OnOffState;
import com.fleetboard.sdk.lib.powerstate.IPowerStateError;
import com.fleetboard.sdk.lib.powerstate.IPowerStateEvent;

/**
 * Important note: to prevent this activity from configuration changes caused by docking/undocking the device <BR>
 * the activity entry in the AndroidManifest.xml must contain this entry:<BR>
 * android:configChanges="uiMode"
 */
public class PowerStateDemoActivity extends Activity {

    private static final String TAG = PowerStateDemoActivity.class.getSimpleName();

    private PowerStateClientCallback fCallback;

    private Switch fPowerStateEventSwitch;
    private TextView fPowerStateEventTimestampTxt;

    private ProgressBar fBatteryLevelProgressBar;
    private TextView fBatteryLevelTxt;

    private RadioButton fPowerStatePoweredUnknownRadio;
    private RadioButton fPowerStatePoweredOffRadio;
    private RadioButton fPowerStatePoweredOnRadio;

    private RadioButton fPowerStateDockedUnknownRadio;
    private RadioButton fPowerStateDockedOffRadio;
    private RadioButton fPowerStateDockedOnRadio;

    private RadioButton fPowerStateIgnitionUnknownRadio;
    private RadioButton fPowerStateIgnitionOffRadio;
    private RadioButton fPowerStateIgnitionOnRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        fCallback = new PowerStateClientCallback();

        getActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_powerstate);

        // switch to register/unregister for/from PowerStateEvents
        fPowerStateEventSwitch = (Switch) findViewById(R.id.powerStateEventSwitch);
        fPowerStateEventSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    registerPowerStateEventListener();
                } else {
                    unregisterPowerStateEventListener();
                    fCallback.onPowerStateChanged(null);
                }
            }
        });
        fPowerStateEventTimestampTxt = (TextView) findViewById(R.id.powerStateEventTimeStampTxt);
        // progress bar to show the current battery level
        fBatteryLevelProgressBar = (ProgressBar) findViewById(R.id.powerStateBatteryLevelProgressBar);
        fBatteryLevelProgressBar.setMax(100);
        fBatteryLevelTxt = (TextView) findViewById(R.id.powerStateBatteryLevelTxt);

        fPowerStatePoweredUnknownRadio = (RadioButton) findViewById(R.id.powerStatePoweredUnknownRadio);
        fPowerStatePoweredOffRadio = (RadioButton) findViewById(R.id.powerStatePoweredOffRadio);
        fPowerStatePoweredOnRadio = (RadioButton) findViewById(R.id.powerStatePoweredOnRadio);

        fPowerStateDockedUnknownRadio = (RadioButton) findViewById(R.id.powerStateDockedUnknownRadio);
        fPowerStateDockedOffRadio = (RadioButton) findViewById(R.id.powerStateDockedOffRadio);
        fPowerStateDockedOnRadio = (RadioButton) findViewById(R.id.powerStateDockedOnRadio);

        fPowerStateIgnitionUnknownRadio = (RadioButton) findViewById(R.id.powerStateIgnitionUnknownRadio);
        fPowerStateIgnitionOffRadio = (RadioButton) findViewById(R.id.powerStateIgnitionOffRadio);
        fPowerStateIgnitionOnRadio = (RadioButton) findViewById(R.id.powerStateIgnitionOnRadio);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onStart()
     */
    @Override
    protected void onStart() {
        Log.i(TAG, "onStart");
        super.onStart();
        try {
            // connect the PowerStateClient to the remote service
            PowerStateClient.INSTANCE.connect(fCallback, this);
        } catch (PowerStateClientException e) {
            Log.e(TAG, "connecting PowerStateClient failed", e);
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
        // disconnect the PowerStateClient from the remote service
        PowerStateClient.INSTANCE.disconnect();
    }

    private void registerPowerStateEventListener() {
        Log.d(TAG, "register PowerStateEventListener");
        try {
            PowerStateClient.INSTANCE.registerPowerStateEventListener();
        } catch (PowerStateClientException e) {
            Log.e(TAG, "register PowerStateEventListener failed");
        }
    }

    private void unregisterPowerStateEventListener() {
        Log.d(TAG, "unregister PowerStateEventListener");
        try {
            PowerStateClient.INSTANCE.unregisterPowerStateEventListener();
        } catch (PowerStateClientException e) {
            Log.e(TAG, "unregister PowerStateEventListener failed");
        }
    }

    /**
     * Implementation of the {@link IPowerStateClientCallback}.
     */
    private class PowerStateClientCallback implements IPowerStateClientCallback {

        private final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy/MM/dd H:mm:ss:SSS");

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onPowerStateChanged(com.fleetboard.platform.externalaccess.lib.powerstate.IPowerStateEvent)
         */
        @Override
        public void onPowerStateChanged(IPowerStateEvent event) {
            if (event != null) {
                Log.i(TAG, "onPowerStateChanged");
                fPowerStateEventTimestampTxt.setText(formatTimestamp(event.getTimestamp()));
                fBatteryLevelProgressBar.setProgress(event.getBatteryLevel());
                fBatteryLevelTxt.setText(String.format("%s%%", event.getBatteryLevel()));
                // powered
                OnOffState powerState = event.getPowerState();
                if (OnOffState.OFF == powerState) {
                    fPowerStatePoweredOffRadio.setChecked(true);
                } else if (OnOffState.ON == powerState) {
                    fPowerStatePoweredOnRadio.setChecked(true);
                } else {
                    fPowerStatePoweredUnknownRadio.setChecked(true);
                }
                // docked
                OnOffState dockState = event.getDockState();
                if (OnOffState.OFF == dockState) {
                    fPowerStateDockedOffRadio.setChecked(true);
                } else if (OnOffState.ON == dockState) {
                    fPowerStateDockedOnRadio.setChecked(true);
                } else {
                    fPowerStateDockedUnknownRadio.setChecked(true);
                }
                // ignition
                OnOffState ignitionState = event.getIgnitionState();
                if (OnOffState.OFF == ignitionState) {
                    fPowerStateIgnitionOffRadio.setChecked(true);
                } else if (OnOffState.ON == ignitionState) {
                    fPowerStateIgnitionOnRadio.setChecked(true);
                } else {
                    fPowerStateIgnitionUnknownRadio.setChecked(true);
                }
            } else {
                fPowerStateEventTimestampTxt.setText("");
                fBatteryLevelProgressBar.setProgress(0);
                fBatteryLevelTxt.setText(String.format("%s%%", 0));
                fPowerStatePoweredUnknownRadio.setChecked(true);
                fPowerStateDockedUnknownRadio.setChecked(true);
                fPowerStateIgnitionUnknownRadio.setChecked(true);
            }
        }

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onError(com.fleetboard.platform.externalaccess.lib.powerstate.PowerStateError)
         */
        @Override
        public void onError(IPowerStateError error) {
            Log.e(TAG, error.getErrorMessage());
            Toast.makeText(PowerStateDemoActivity.this, "onError: " + error.getErrorMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onSuspend()
         */
        @Override
        public void onSuspend() {
            Log.i(TAG, "onSuspend");
            Toast.makeText(PowerStateDemoActivity.this, "onSuspend", Toast.LENGTH_SHORT).show();
        }

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onWakeup()
         */
        @Override
        public void onWakeup() {
            Log.i(TAG, "onWakeup");
            Toast.makeText(PowerStateDemoActivity.this, "onWakeup", Toast.LENGTH_SHORT).show();
        }

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onServiceDisconnected()
         */
        @Override
        public void onPowerStateClientDisconnected() {
            Log.i(TAG, "onServiceDisconnected");
            // disable the registration switch
            fPowerStateEventSwitch.setEnabled(false);
            // trigger the listener unregistration
            fPowerStateEventSwitch.setChecked(false);
            // reset the controls
            onPowerStateChanged(null);
        }

        /* (non-Javadoc)
         * @see com.fleetboard.platform.externalaccess.lib.android.powerstate.IPowerStateClientCallback#onServiceConnected()
         */
        @Override
        public void onPowerStateClientConnected() {
            Log.i(TAG, "onServiceConnected");
            // reset the controls
            onPowerStateChanged(null);
            // enable the registration switch
            fPowerStateEventSwitch.setEnabled(true);
            // trigger the listener registration
            fPowerStateEventSwitch.setChecked(true);
            registerPowerStateEventListener();
        }

        private String formatTimestamp(long timestamp) {
            return TIMESTAMP_FORMAT.format(new Date(timestamp));
        }

    }
}
