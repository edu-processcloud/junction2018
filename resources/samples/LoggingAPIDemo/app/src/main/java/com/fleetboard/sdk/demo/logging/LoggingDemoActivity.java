package com.fleetboard.sdk.demo.logging;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fleetboard.sdk.lib.android.log.Log;

public class LoggingDemoActivity extends Activity {

    private Spinner fLevel;
    private EditText fMessage;

    private final Map<String, Integer> fLogLevelNameToNumber = new HashMap<String, Integer>();

    private static final String TAG = LoggingDemoActivity.class.getSimpleName();

    {
        fLogLevelNameToNumber.put("DEBUG", 4);
        fLogLevelNameToNumber.put("INFO", 3);
        fLogLevelNameToNumber.put("WARN", 2);
        fLogLevelNameToNumber.put("ERROR", 1);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);

        fLevel = (Spinner) findViewById(R.id.logging_selectLevelSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.logging_LogLevels, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fLevel.setAdapter(adapter);

        fMessage = (EditText) findViewById(R.id.logging_inputMsgText);
    }

    public void sendLog(final View view) {
        final int level = fLogLevelNameToNumber.get(fLevel.getSelectedItem().toString());
        final String message = fMessage.getText().toString();
        switch (level) {
            case 4:
                Log.d(TAG, message);
                Toast.makeText(LoggingDemoActivity.this,
                        "DEBUG" + ": \"" + message +"\" logged" , Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Log.i(TAG, message);
                Toast.makeText(LoggingDemoActivity.this,
                        "INFO" + ": \"" + message +"\" logged" , Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Log.w(TAG, message);
                Toast.makeText(LoggingDemoActivity.this,
                        "WARN" + ": \"" + message +"\" logged" , Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Log.e(TAG, message);
                Toast.makeText(LoggingDemoActivity.this,
                        "ERROR" + ": \"" + message +"\" logged" , Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(LoggingDemoActivity.this,
                        "no message logged", Toast.LENGTH_SHORT).show();
        }
    }
}
