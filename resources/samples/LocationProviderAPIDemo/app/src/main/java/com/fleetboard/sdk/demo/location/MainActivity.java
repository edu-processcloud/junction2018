package com.fleetboard.sdk.demo.location;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fleetboard.sdk.lib.android.common.SDKInitializer;
import com.fleetboard.sdk.lib.android.common.SDKInitializerException;
import com.fleetboard.sdk.lib.android.log.Log;
import com.fleetboard.sdk.lib.android.version.SdkVersionUtils;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getName();

    public static Intent getMainActivityIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getActionBar().setDisplayHomeAsUpEnabled(false);

        Button locationBtn = (Button) findViewById(R.id.mainLocationButton);
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(LocationDemoActivity.class);
            }
        });

        // initialize the FleetBoard SDK
        try {
            SDKInitializer.INSTANCE.init(getApplicationContext());
            TextView sdkLibVersion = (TextView) findViewById(R.id.sdkLibVersion);
            sdkLibVersion.setText(SdkVersionUtils.getLibVersion(this).getVersionCode());
            TextView platformSDKVersion = (TextView) findViewById(R.id.platformLibVersion);
            platformSDKVersion.setText(SdkVersionUtils.getPlatformVersion(this).getVersionCode());
        } catch (Exception e) {
            Log.e(TAG, "Initialization the SDK failed.", e);
            // show exception
            TextView errorMessageTxt = (TextView) findViewById(R.id.mainErrorMessageTxt);
            errorMessageTxt.setVisibility(TextView.VISIBLE);
            errorMessageTxt.setText("ERROR: " + e.getMessage());
            // disable API demos
            locationBtn.setEnabled(false);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // terminate the FleetBoard SDK
        try {
            SDKInitializer.INSTANCE.terminate();
        } catch (SDKInitializerException e) {
            Log.e(TAG, "Termination of the SDK failed.", e);
        }
    }

    private void startActivity(Class<? extends Activity> activityClass) {
        Intent intent = new Intent(this, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}