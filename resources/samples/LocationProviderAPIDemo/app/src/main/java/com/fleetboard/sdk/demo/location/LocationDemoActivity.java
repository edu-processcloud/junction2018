package com.fleetboard.sdk.demo.location;

import android.app.Activity;
import android.content.Context;
import android.location.*;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.fleetboard.sdk.lib.android.log.Log;

public class LocationDemoActivity extends Activity {

    private static final String TAG = LocationDemoActivity.class.getSimpleName();
    private static final Criteria CRITERIA = new Criteria() {
        {
            this.setAccuracy(Criteria.ACCURACY_FINE);
            this.setAltitudeRequired(true);
            this.setBearingAccuracy(Criteria.ACCURACY_HIGH);
            this.setBearingRequired(true);
            this.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
            this.setSpeedAccuracy(Criteria.ACCURACY_HIGH);
            this.setSpeedRequired(true);
            this.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        }
    };

    // used for GPS location
    private LocationManager fLocationManager;
    private DemoLocationListener fLocationListener;
    private TextView fGpsLatTxt;
    private TextView fGpsLongTxt;
    private TextView fGpsSpeedTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_location);

        fGpsLatTxt = (TextView) findViewById(R.id.locationGpsLatTxt);
        fGpsLongTxt = (TextView) findViewById(R.id.locationGpsLongTxt);
        fGpsSpeedTxt = (TextView) findViewById(R.id.locationGpsSpeedTxt);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "in Switch: " + item.getItemId());
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(MainActivity.getMainActivityIntent(this));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
        initLocationManager();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
        releaseLocationManager();
    }

    private void initLocationManager() {
        Log.d(TAG, "initLocationManager");
        fLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        fLocationListener = new DemoLocationListener();
        fLocationManager.requestLocationUpdates(0, 0f, CRITERIA, fLocationListener, null);
    }

    private void releaseLocationManager() {
        Log.d(TAG, "releaseLocationManager");
        fLocationManager.removeUpdates(fLocationListener);
        fLocationListener = null;
        fLocationManager = null;
    }

    private void updateLocation(Location loc) {
        String latText = "";
        String longText = "";
        String speedText = "";
        if (loc != null) {
            latText = Location.convert(loc.getLatitude(), Location.FORMAT_SECONDS);
            longText = Location.convert(loc.getLongitude(), Location.FORMAT_SECONDS);
            // convert m/s to km/h and round to full km/h
            speedText = String.format("%s km/h", (int) ((loc.getSpeed() * 3.6f) + 0.5));
        }
        fGpsLatTxt.setText(latText);
        fGpsLongTxt.setText(longText);
        fGpsSpeedTxt.setText(speedText);
    }

    private class DemoLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged: " + location);
            updateLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "LocationListener.onStatusChanged: provider = " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "LocationListener.onProviderEnabled: provider = " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "LocationListener.onProviderDisabled: provider = " + provider);
        }
    }

}
