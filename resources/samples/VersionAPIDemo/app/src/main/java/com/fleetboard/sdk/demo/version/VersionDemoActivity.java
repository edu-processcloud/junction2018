package com.fleetboard.sdk.demo.version;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.TextView;

import com.fleetboard.sdk.lib.android.log.Log;
import com.fleetboard.sdk.lib.android.version.SdkVersionUtils;
import com.fleetboard.sdk.lib.version.SdkVersion;

public class VersionDemoActivity extends Activity {

	private static final String TAG = VersionDemoActivity.class.getSimpleName();

	private TextView fPlatformVersionTxt;
	private TextView fLibVersionTxt;
	private RadioButton fCompatibleYesRadio;
	private RadioButton fCompatibleNoRadio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate");
		super.onCreate(savedInstanceState);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_version);

		fPlatformVersionTxt = (TextView) findViewById(R.id.platformVersionTxt);
		fLibVersionTxt = (TextView) findViewById(R.id.libVersionTxt);
		fCompatibleYesRadio = (RadioButton) findViewById(R.id.versionCompatibleYesRadio);
		fCompatibleNoRadio = (RadioButton) findViewById(R.id.versionCompatibleNoRadio);

		update();
	}

	private void update() {
		SdkVersion platformVersion = SdkVersionUtils.getPlatformVersion(this);
		fPlatformVersionTxt.setText(platformVersion.getVersionCode());

		SdkVersion libVersion = SdkVersionUtils.getLibVersion(this);
		fLibVersionTxt.setText(libVersion.getVersionCode());

		boolean compatible = SdkVersionUtils.isLibVersionCompatibility(this);
		fCompatibleYesRadio.setChecked(compatible);
		fCompatibleNoRadio.setChecked(!compatible);
	}

}
