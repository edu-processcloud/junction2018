package com.movesense.samples.sensorsample;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsResponseListener;
import com.movesense.mds.MdsSubscription;
import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.scan.ScanSettings;

import java.util.ArrayList;

import rx.Subscription;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener  {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    // MDS
    private Mds mMds;
    public static final String URI_CONNECTEDDEVICES = "suunto://MDS/ConnectedDevices";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    public static final String SCHEME_PREFIX = "suunto://";

    // BleClient singleton
    static private RxBleClient mBleClient;

    // UI
    private ListView mScanResultListView;
    private ArrayList<MyScanResult> mScanResArrayList = new ArrayList<>();
    ArrayAdapter<MyScanResult> mScanResArrayAdapter;

    // Sensor subscription
    static private String URI_MEAS_ACC_13 = "/Meas/Acc/13";
    static private String URI_MEAS_GYRO_13 = "/Meas/Gyro/13";
    private MdsSubscription mdsSubscriptionAcc;
    private MdsSubscription mdsSubscriptionGyro;
    private String subscribedDeviceSerial;
    private ArrayList<Double> move = new ArrayList<Double>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init Scan UI
        mScanResultListView = (ListView)findViewById(R.id.listScanResult);
        mScanResArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mScanResArrayList);
        mScanResultListView.setAdapter(mScanResArrayAdapter);
        mScanResultListView.setOnItemLongClickListener(this);
        mScanResultListView.setOnItemClickListener(this);

        // Make sure we have all the permissions this app needs
        requestNeededPermissions();

        // Initialize Movesense MDS library
        initMds();
    }

    private RxBleClient getBleClient() {
        // Init RxAndroidBle (Ble helper library) if not yet initialized
        if (mBleClient == null)
        {
            mBleClient = RxBleClient.create(this);
        }

        return mBleClient;
    }

    private void initMds() {
        mMds = Mds.builder().build(this);
    }

    void requestNeededPermissions()
    {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);

        }
    }

    Subscription mScanSubscription;
    public void onScanClicked(View view) {
        findViewById(R.id.buttonScan).setVisibility(View.GONE);
        findViewById(R.id.buttonScanStop).setVisibility(View.VISIBLE);

        // Start with empty list
        mScanResArrayList.clear();
        mScanResArrayAdapter.notifyDataSetChanged();

        mScanSubscription = getBleClient().scanBleDevices(
                new ScanSettings.Builder()
                        // .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) // change if needed
                        // .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) // change if needed
                        .build()
                // add filters if needed
        )
                .subscribe(
                        scanResult -> {
                            Log.d(LOG_TAG,"scanResult: " + scanResult);

                            // Process scan result here. filter movesense devices.
                            if (scanResult.getBleDevice()!=null &&
                                    scanResult.getBleDevice().getName() != null &&
                                    scanResult.getBleDevice().getName().startsWith("Movesense")) {

                                // replace if exists already, add otherwise
                                MyScanResult msr = new MyScanResult(scanResult);
                                if (mScanResArrayList.contains(msr))
                                    mScanResArrayList.set(mScanResArrayList.indexOf(msr), msr);
                                else
                                    mScanResArrayList.add(0, msr);

                                mScanResArrayAdapter.notifyDataSetChanged();
                            }
                        },
                        throwable -> {
                            Log.e(LOG_TAG,"scan error: " + throwable);
                            // Handle an error here.

                            // Re-enable scan buttons, just like with ScanStop
                            onScanStopClicked(null);
                        }
                );
    }

    public void onScanStopClicked(View view) {
        if (mScanSubscription != null)
        {
            mScanSubscription.unsubscribe();
            mScanSubscription = null;
        }

        findViewById(R.id.buttonScan).setVisibility(View.VISIBLE);
        findViewById(R.id.buttonScanStop).setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position < 0 || position >= mScanResArrayList.size())
            return;

        MyScanResult device = mScanResArrayList.get(position);
        if (!device.isConnected()) {
            // Stop scanning
            onScanStopClicked(null);

            // And connect to the device
            connectBLEDevice(device);
        }
        else {
            // Device is connected, trigger showing /Info
            subscribeToSensorAcc(device.connectedSerial,URI_MEAS_ACC_13);
            subscribeToSensorGyro(device.connectedSerial,URI_MEAS_GYRO_13);

        }
    }

    private void subscribeToSensorAcc(String connectedSerial, String url) {
        // Clean up existing subscription (if there is one)
        if (mdsSubscriptionAcc != null) {
            unsubscribe(mdsSubscriptionAcc);
        }

        // Build JSON doc that describes what resource and device to subscribe
        // Here we subscribe to 13 hertz accelerometer data
        StringBuilder sb = new StringBuilder();
        String strContract = sb.append("{\"Uri\": \"").append(connectedSerial).append(url).append("\"}").toString();
        Log.d(LOG_TAG, strContract);
        final View sensorUI = findViewById(R.id.sensorUI);

        subscribedDeviceSerial = connectedSerial;

        mdsSubscriptionAcc = mMds.builder().build(this).subscribe(URI_EVENTLISTENER,
                strContract, new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {
                        Log.d(LOG_TAG, "onNotification(): " + data);

                        // If UI not enabled, do it now
                        if (sensorUI.getVisibility() == View.GONE)
                            sensorUI.setVisibility(View.VISIBLE);

                            AccDataResponse accResponse = new Gson().fromJson(data, AccDataResponse.class);
                            if (accResponse != null && accResponse.body.array.length > 0) {

                                String accStr =
                                        String.format("%.02f, %.02f, %.02f", accResponse.body.array[0].x, accResponse.body.array[0].y, accResponse.body.array[0].z);

                                ((TextView) findViewById(R.id.sensorMsg)).setText(accStr);

                                final ProgressBar pgBXl = (ProgressBar) findViewById(R.id.progressXl);
                                final ProgressBar pgBXr = (ProgressBar) findViewById(R.id.progressXr);

                                final ProgressBar pgBYl = (ProgressBar) findViewById(R.id.progressYl);
                                final ProgressBar pgBYr = (ProgressBar) findViewById(R.id.progressYr);

                                final ProgressBar pgBZl = (ProgressBar) findViewById(R.id.progressZl);
                                final ProgressBar pgBZr = (ProgressBar) findViewById(R.id.progressZr);


                                pgBXl.setProgress(((int) accResponse.body.array[0].x) * 10);
                                pgBXr.setProgress(((int) accResponse.body.array[0].x) * -10);

                                pgBYl.setProgress(((int) accResponse.body.array[0].y) * 10);
                                pgBYr.setProgress(((int) accResponse.body.array[0].y) * -10);

                                pgBZl.setProgress(((int) accResponse.body.array[0].z) * 10);
                                pgBZr.setProgress(((int) accResponse.body.array[0].z) * -10);
                            }

                    }

                    @Override
                    public void onError(MdsException error) {
                        Log.e(LOG_TAG, "subscription onError(): ", error);
                        unsubscribe(mdsSubscriptionAcc);
                    }
                });

    }


    private void subscribeToSensorGyro(String connectedSerial, String url) {
        // Clean up existing subscription (if there is one)
        if (mdsSubscriptionGyro != null) {
            unsubscribe(mdsSubscriptionGyro);
        }

        // Build JSON doc that describes what resource and device to subscribe
        // Here we subscribe to 13 hertz accelerometer data
        StringBuilder sb = new StringBuilder();
        String strContract = sb.append("{\"Uri\": \"").append(connectedSerial).append(url).append("\"}").toString();
        Log.d(LOG_TAG, strContract);
        final View sensorUI = findViewById(R.id.sensorUI);

        subscribedDeviceSerial = connectedSerial;

        mdsSubscriptionGyro = mMds.builder().build(this).subscribe(URI_EVENTLISTENER,
                strContract, new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {
                        Log.d(LOG_TAG, "onNotification(): " + data);

                        // If UI not enabled, do it now
                        if (sensorUI.getVisibility() == View.GONE)
                            sensorUI.setVisibility(View.VISIBLE);

                            GyroDataResponse gyroResponse = new Gson().fromJson(data, GyroDataResponse.class);
                            if (gyroResponse != null && gyroResponse.body.array.length > 0) {

                                String accStr =
                                        String.format("%.02f, %.02f, %.02f", gyroResponse.body.array[0].x, gyroResponse.body.array[0].y, gyroResponse.body.array[0].z);

                                ((TextView) findViewById(R.id.sensorMsg)).setText(accStr);

                                final ProgressBar pgBXl = (ProgressBar) findViewById(R.id.progressGXl);
                                final ProgressBar pgBXr = (ProgressBar) findViewById(R.id.progressGXr);

                                final ProgressBar pgBYl = (ProgressBar) findViewById(R.id.progressGYl);
                                final ProgressBar pgBYr = (ProgressBar) findViewById(R.id.progressGYr);

                                final ProgressBar pgBZl = (ProgressBar) findViewById(R.id.progressGZl);
                                final ProgressBar pgBZr = (ProgressBar) findViewById(R.id.progressGZr);


                                pgBXl.setProgress(((int) gyroResponse.body.array[0].x) * 1);
                                pgBXr.setProgress(((int) gyroResponse.body.array[0].x) * -1);

                                pgBYl.setProgress(((int) gyroResponse.body.array[0].y) * 1);
                                pgBYr.setProgress(((int) gyroResponse.body.array[0].y) * -1);

                                pgBZl.setProgress(((int) gyroResponse.body.array[0].z) * 1);
                                pgBZr.setProgress(((int) gyroResponse.body.array[0].z) * -1);

                                move.add(gyroResponse.body.array[0].y);
                                if(move.size()> 10) {
                                    double min = 0;
                                    double max = 0;
                                    for (int j = 0; j < move.size(); j++) {
                                        if (move.get(j) > max)
                                            max = move.get(j);
                                        if (move.get(j) < min)
                                            min = move.get(j);
                                    }
                                    move.remove(0);
                                    final View textGyro = findViewById(R.id.gyroText);
                                    double delta = (Math.abs(min) + max);
                                    if( delta > 160 ) {
                                        textGyro.setBackgroundColor(Color.RED);
                                    }else if( delta > 110 ){
                                        textGyro.setBackgroundColor(Color.YELLOW);
                                    }else{
                                        textGyro.setBackgroundColor(Color.WHITE);
                                    }
                                    Log.d(LOG_TAG, "delta : " + String.valueOf(delta));
                                }





                            }
                    }

                    @Override
                    public void onError(MdsException error) {
                        Log.e(LOG_TAG, "subscription onError(): ", error);
                        unsubscribe(mdsSubscriptionAcc);
                        unsubscribe(mdsSubscriptionGyro);
                    }
                });

    }



    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (position < 0 || position >= mScanResArrayList.size())
            return false;

        MyScanResult device = mScanResArrayList.get(position);

        // unsubscribe if there
        Log.d(LOG_TAG, "onItemLongClick, " + device.connectedSerial + " vs " + subscribedDeviceSerial);
        if (device.connectedSerial.equals(subscribedDeviceSerial)) {
            unsubscribe(mdsSubscriptionAcc);
            unsubscribe(mdsSubscriptionGyro);
        }

        Log.i(LOG_TAG, "Disconnecting from BLE device: " + device.macAddress);
        mMds.disconnect(device.macAddress);

        return true;
    }

    private void connectBLEDevice(MyScanResult device) {
        RxBleDevice bleDevice = getBleClient().getBleDevice(device.macAddress);

        Log.i(LOG_TAG, "Connecting to BLE device: " + bleDevice.getMacAddress());
        mMds.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {

            @Override
            public void onConnect(String s) {
                Log.d(LOG_TAG, "onConnect:" + s);
            }

            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                for (MyScanResult sr : mScanResArrayList) {
                    if (sr.macAddress.equalsIgnoreCase(macAddress)) {
                        sr.markConnected(serial);
                        break;
                    }
                }
                mScanResArrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(MdsException e) {
                Log.e(LOG_TAG, "onError:" + e);

                showConnectionError(e);
            }

            @Override
            public void onDisconnect(String bleAddress) {

                Log.d(LOG_TAG, "onDisconnect: " + bleAddress);
                for (MyScanResult sr : mScanResArrayList) {
                    if (bleAddress.equals(sr.macAddress))
                    {
                        // unsubscribe if was subscribed
                        if (sr.connectedSerial != null && sr.connectedSerial.equals(subscribedDeviceSerial)) {
                            unsubscribe(mdsSubscriptionAcc);
                            unsubscribe(mdsSubscriptionGyro);
                        }
                        sr.markDisconnected();
                    }
                }
                mScanResArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void showConnectionError(MdsException e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Connection Error:")
                .setMessage(e.getMessage());

        builder.create().show();
    }

    private void unsubscribe(MdsSubscription mdsSubscription) {
        if (mdsSubscription != null) {
            mdsSubscription.unsubscribe();
            mdsSubscription = null;
        }

        subscribedDeviceSerial = null;

        // If UI not invisible, do it now
        final View sensorUI = findViewById(R.id.sensorUI);
        if (sensorUI.getVisibility() != View.GONE)
            sensorUI.setVisibility(View.GONE);

    }
    public void onUnsubscribeClicked(View view) {
        unsubscribe(mdsSubscriptionAcc);
        unsubscribe(mdsSubscriptionGyro);
    }
}
